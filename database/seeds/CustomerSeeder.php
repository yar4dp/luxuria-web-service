<?php

use App\Customer;
use App\User;
use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 50;

        for ($i = 0; $i < $limit; $i++) {

            $user = User::create([
                'email' => $faker->safeEmail,
                'password' => bcrypt('rahasia'),
                'role' => 'customer',
            ]);

            Customer::create([
                'user_id' => $user->id,
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'birth_date' => $faker->date('Y-m-d'),
                'address' => $faker->address,
                'phone_number' => $faker->phoneNumber,
            ]);

        }
    }
}
