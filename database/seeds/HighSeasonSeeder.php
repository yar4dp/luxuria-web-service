<?php

use App\HighSeason;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class HighSeasonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-12-27'),
            'additional_rate' => 0.10,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-12-28'),
            'additional_rate' => 0.10,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-12-29'),
            'additional_rate' => 0.10,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-12-30'),
            'additional_rate' => 0.20,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2017-12-31'),
            'additional_rate' => 0.30,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2018-01-01'),
            'additional_rate' => 0.30,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2018-01-02'),
            'additional_rate' => 0.20,
        ]);
        HighSeason::create([
            'calendar_date' => Carbon::parse('2018-01-03'),
            'additional_rate' => 0.10,
        ]);
    }
}
