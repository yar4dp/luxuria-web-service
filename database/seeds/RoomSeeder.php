<?php

use App\Room;
use Illuminate\Database\Seeder;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = '/img/rooms/';

        Room::create([
            'name' => 'Deluxe Room',
            'description' => 'Offering 69 square meters of inviting space, what sets a Luxuria Deluxe Room apart from a Luxuria Guest Room is its size and dramatic double-aspect windows that afford our guests with panoramic views and abundant natural light. Stylish décor and contemporary furnishings provide a relaxing retreat for both business and leisure travelers. Luxurious Le Labo bath amenities can be enjoyed in the Japanese inspired marble spa bathroom with separate rain shower and bath tub. Each Deluxe luxury hotel room features a well thought-out work area, high speed Internet connection, interactive flat screen television and media panels.',
            'size' => '69 sq.m / 743 sq.ft.',
            'bed_type' => '1 King or 2 Double',
            'view' => 'City Skyline or Golf Course',
            'rate' => 1120,
            'image_url' => $path . '01.jpg',
        ]);
        Room::create([
            'name' => 'Presidential Suite',
            'description' => 'For guests with the most discerning tastes and high regard for luxury living, our 303 sq.m. Luxuria Presidential Suite features a separate living area, long dining table, executive work desk, kitchenette, private massage room, gym and spacious spa marble bathroom with separate rain shower and large bath tub, along with spectacular skyline views of Ukheegrego. Our guests book the Presidential Suite for special occasions, private meetings and to mark moments of significance. Wired and wireless high speed Internet connection and advanced in-room technology are also on hand. Guests also enjoy exclusive access to the Luxuria Gold Lounge offering daily breakfast, evening hors d’ oeuvres, all-day refreshments, suit or dress pressing and complimentary use of the boardroom for two hours.',
            'size' => '303 sq.m / 3261 sq.ft.',
            'bed_type' => '1 King',
            'view' => 'City Skyline',
            'rate' => 1410,
            'image_url' => $path . '02.jpg',
        ]);
        Room::create([
            'name' => 'Luxuria Gold Room',
            'description' => 'The Luxuria Gold room provides a comfortable 49 square meters of refined ambience, with excellent views of Ukheegrego City or Senayan Golf Course. Guests are treated to elegantly appointed accommodations, discrete and efficient butler service, and exclusive access to the Luxuria Gold Lounge offering daily breakfast, evening hors d’ oeuvres, all-day refreshments, suit or dress pressing and complimentary use of the boardroom for two hours. Aside from the added benefits, what our repeat guests enjoy the most during their stay is the world-class yet unobtrusive, genuine service provided by the Luxuria Gold team – perfect for those in search of privacy and calm.',
            'size' => '49 sq.m / 527 sq.ft.',
            'bed_type' => '1 King or 2 Double',
            'view' => 'City Skyline or Golf Course',
            'rate' => 1725,
            'image_url' => $path . '03.jpg',
        ]);
        Room::create([
            'name' => 'Luxuria Gold Deluxe Room',
            'description' => 'The Luxuria Gold Deluxe room provides an abundant 65 square meters of refined ambience, with excellent views of Ukheegrego City or Senayan Golf Course. Guests are treated to elegantly appointed accommodations, discrete and efficient butler service, and exclusive access to the Luxuria Gold Lounge offering daily breakfast, evening hors d’ oeuvres, all-day refreshments, suit or dress pressing and complimentary use of the boardroom for two hours. Aside from the added benefits, what our repeat guests enjoy the most during their stay is the world-class yet unobtrusive, genuine service provided by the Luxuria Gold team – perfect for those in search of privacy and calm.',
            'size' => '65 sq.m / 700 sq.ft.',
            'bed_type' => '1 King',
            'view' => 'City Skyline',
            'rate' => 2010,
            'image_url' => $path . '04.jpg',
        ]);
        Room::create([
            'name' => 'One Bedroom Sky Suite',
            'description' => 'Luxuria offers our travelers something unique - a true home-away-from-home experience. Our Sky Suites are the perfect choice for long-stay suite-style living, offering guests a spacious 85 sq.m suite with kitchenette, dining area, living area and bedroom. Well thought-out work area, high speed Internet connection, interactive flat screen television and media panels are available to provide each guest an efficient and comfortable stay experience. Our guests will be sure to feel extremely comfortable and connected to what is important to them when away from home for either a few nights or a few months.',
            'size' => '85 sq.m / 915 sq.ft.',
            'bed_type' => '1 King',
            'view' => 'City Skyline or Golf Course',
            'rate' => 2720,
            'image_url' => $path . '05.jpg',
        ]);
    }
}
