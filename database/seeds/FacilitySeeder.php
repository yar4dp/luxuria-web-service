<?php

use App\Facility;
use Illuminate\Database\Seeder;

class FacilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = '/img/facilities/';

        Facility::create([
            'name' => 'Weddings',
            'description' => '<p>Luxuria Hotel is where “happily-ever-after” begins in grand style. From the Ballroom\'s exquisite details, sophisticated interiors and adjacent VIP room, brides and their grooms have a wealth of elegant options for their special day.</p><p>Intimate or grand, traditional or avant-garde, Luxuria Hotel is the premier choice for your wedding.</p><p>Whether you opt for a gourmet meal followed by dancing or a cocktail reception with innovative hors d’oeuvres, Luxuria Hotel’s talented culinary team exceeds expectations. Our exclusive partner, One Heart Wedding, offers the highest level of bespoke services and wedding concepts unique only to Luxuria Hotel, to create the day of your dreams.</p>',
            'image_url' => $path . 'weddings.jpg',
        ]);
        Facility::create([
            'name' => 'Meetings',
            'description' => '<p>Reflecting the expectations of a world-class city, Luxuria Hotel is proud to offer professional meeting facilities supported by a comprehensive array of professional and state-of-the-art services in our Ukheegrego meeting and event venues. Our event planners offer an expert’s knowledge in planning meetings that are unparalleled successes.</p><p>The hotel’s meeting venues at the gateway to the lush, lively business district of Ukheegrego include flexible meeting rooms, dedicated boardrooms, deluxe guest rooms and suites, a magnificent ballroom featuring a spacious foyer and dedicated arrival and parking for over 700 cars, plus a VIP Meeting Room that is ideal for high level meetings or sensitive negotiation sessions. Designed to host an executive meeting for 10 or a professional conference for 500, we make certain everything you need is at your fingertips.</p>',
            'image_url' => $path . 'meetings.jpg',
        ]);
        Facility::create([
            'name' => 'Spa',
            'description' => '<p>A sanctuary right in the heart of Ukheegrego. Willow Stream Spa is among the city’s most luxurious spa retreats. A haven where you can slow down, catch your breath, revitalize and indulge in a calming experience to restore your energy in one of the 9 fully-equipped treatment rooms.</p><p>Designed to promote inner peace and serenity, our Ukheegrego day spa boasts 900 square meters of soothing spaces and a wide range of natural therapies and treatments that allow you to relax and truly indulge the body, mind and spirit.  </p><p>Every element of your Ukheegrego luxury spa experience – from the spacious relaxation areas, to the well-appointed spa rooms – will remind you to take time and enjoy life’s simple pleasures.</p> <p>In addition to our Willow Stream Spa, we also offer exclusive benefits for our Health Club members and in-house guests, where they can enjoy the Fitness Center, offering the latest fitness machines, state-of-the-art equipment and a Fitness Studio, as well as the outdoor swimming pool, children’s splash pool and whirlpool to round off your wellness experience.</p>',
            'image_url' => $path . 'spa.jpg',
        ]);
        Facility::create([
            'name' => 'Business Services',
            'description' => '<p>Luxuria Hotel offers a full range of business services to our guests and to residents of Ukheegrego, including high-speed wireless internet access throughout the hotel, as well as the Lobby Lounge, Sapori Deli & Cafe, pool side and at the health club.</p><p>Completing the Ukheegrego district, we offer state-of-the-art business services available through our Business Centre, designed to meet and exceed the needs of busy executives. Whether you’re attending a Luxuria Hotel convention or simply taking a working vacation, our Business Centre offers everything you need to be productive, including 3 well-appointed boardrooms, computer stations with high-speed Internet access, secretarial, courier, printing, photocopying and facsimile services, and also IT Support.</p>',
            'image_url' => $path . 'business-services.jpg',
        ]);
        Facility::create([
            'name' => 'Fitness Gym',
            'description' => '<p>Our inviting outdoor pool is the 625 square meter state-of-the-art gym offering a stunning view of the 4th floor roof top gardens. It features the latest Technogym equipment and a full team of trained fitness professionals.</p><p>Dedicated to promoting a healthy lifestyle, our fitness centre colleagues welcome you to ask questions, try new exercises and experience all that our health centre has to offer.</p><p>The Fitness Centre is open 24 hours. Personal trainers are available to assist from 7:00 a.m. - 5:00 p.m.</p>',
            'image_url' => $path . 'fitness-gym.jpg',
        ]);
        Facility::create([
            'name' => 'Pool',
            'description' => '<p>Among our many amenities is a fourth-floor outdoor swimming pool with sun terrace and leisure facilities, where a full range of services are available. Pool towels can be obtained from the attendants.</p>',
            'image_url' => $path . 'pool.jpg',
        ]);
        Facility::create([
            'name' => 'Luxury Retail',
            'description' => '<p>The hotel has a private luxury retail at hall room that sells a lot of handcraft merchandises. This exclusive luxury retail is open from 6:00 a.m. to 11:00 p.m. daily.</p>',
            'image_url' => $path . 'luxury-retail.jpg',
        ]);
        Facility::create([
            'name' => 'Art Gallery & Arcade',
            'description' => '<p>Sunrise Art Gallery exhibits both burgeoning as well as renowned international and local artworks which focused primarily on contemporary art. Our sister store Sunrise Arcade features beautiful Indonesian and Japanese handicrafts, accessories as well as smaller artworks that customers will love.</p><p>Location: Level 2, Luxuria Hotel</p><p><b>Hours of Operation:  </b><br/>08:00 – 20:00  </p><p><b> Dress Code: </b><br/> Smart Casual   </p><p><b>Reservations: </b><br/> For more information, please call 202-555-0128</p>',
            'image_url' => $path . 'art-gallery.jpg',
        ]);
        Facility::create([
            'name' => 'High Class Cafe',
            'description' => '<p>The newest addition at Luxuria Hotel, a high class cafe that combines the passion of coffee and art. It boasts an extensive range of fine coffee as well as Sake and Spirits of great quality and price. Each guest can expect to receive the highest service from the specialist that will help them to select the perfect coffee for any occasion. The shop also hosts a range of tasting evening every week.</p><p>Don’t miss their special offers from Buy 1 Get 1 or 15% for certain products during this opening period while stocks last.</p><p>Location: Level B1, Luxuria Hotel</p><p><b>Hours of Operation:  </b><br/>10:00 - 22:00 </p><p>  <b>Dress Code:  </b><br/>Smart casual   </p><p><b>Reservations: </b> <br/>For more information or reservations, please call 202-555-0184 ext.3097 </p>',
            'image_url' => $path . 'high-class-cafe.jpg',
        ]);
    }
}
