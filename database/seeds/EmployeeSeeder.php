<?php

use App\Employee;
use App\User;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $user = User::create([
            'email' => 'employee@luxuria.hotel',
            'password' => bcrypt('rahasia'),
            'role' => 'employee',
        ]);

        Employee::create([
            'user_id' => $user->id,
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'phone_number' => $faker->phoneNumber,
        ]);
    }
}
