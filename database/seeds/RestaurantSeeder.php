<?php

use App\Restaurant;
use Illuminate\Database\Seeder;

class RestaurantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = '/img/restaurants/';

        Restaurant::create([
            'name' => 'The Dwarf Hound',
            'description' => '<p>The Dwarf Hound is the hotel’s all-day-dining restaurants, offering multi-cultural cuisine served on its ‘Culinary Theater’ stations and buffet spread. A contemporary restaurant for breakfast, lunch and dinner with a lively atmosphere.</p>
<p>The Sunday Brunch is one of the best in the city, offering extended buffet spread, including pop-up stations serving brunch favorites like eggs benedict, foie gras, waffles and pancake cooked to order, seafood bar as well as pass around dishes to your table.</p>',
            'house_of_operation' => '06:00 - 10:30 (Breakfast)<br/>
12:00 - 14:30 (Lunch)<br/>
18:00 - 22:00 (Dinner)',
            'dress_code' => 'Smart casual',
            'how_to_reserve' => 'Recommended',
            'menu_list_url' => null,
            'image_url' => $path . '01.jpg',
        ]);
        Restaurant::create([
            'name' => 'The Amber Ambience',
            'description' => '<p>Offering spectacular views of the Ukheegrego city skyline with service that is meticulous and unobtrusive, The Amber Ambience exudes an ambience that is dynamic, chic and stylish. This enticing bar offers creative ‘Culinary Cocktails’ and unique presentations. We bring all the essence of the kitchen to the glass; with house made infusions, fresh juices, muddled fruit, infused syrups, earthy spices and savory herbs. Our mixologist creates bespoke cocktails which become an integral part of the atmosphere.</p>
<p>Once night descends the space evokes the elements of sounds – vision – style in a refined lounge concept where a magnetic vibe allows you to chill for hours by the outdoor space while taking in the view of the bustling city of Ukheegrego.</p>',
            'house_of_operation' => '17:00 – 01:00 (Monday - Sunday)',
            'dress_code' => 'Smart casual',
            'how_to_reserve' => 'Required',
            'menu_list_url' => null,
            'image_url' => $path . '02.jpg',
        ]);
        Restaurant::create([
            'name' => 'The Copper Chamber',
            'description' => '<p>The hotel’s lobby will offer a sophisticated lounge where guests can relax and enjoy light bites, tea and coffee throughout the day. Building on Luxuria’s tradition of celebrating Afternoon Tea, the menu offerings and service are unrivalled in Ukheegrego.</p>',
            'house_of_operation' => '9:00 – 21:00',
            'dress_code' => 'Smart casual',
            'how_to_reserve' => 'Suggested but not required',
            'menu_list_url' => null,
            'image_url' => $path . '03.jpg',
        ]);
        Restaurant::create([
            'name' => 'The Comet House',
            'description' => '<p>The Comet House delivers classics perfected cocktails, masterfully crafted using locally sourced ingredients mixed with professional techniques. A sleek yet relaxed ambience with emphasis on lounging and socializing — an enticing location where guests and local residents enjoy spending an evening drinking with friends in an upmarket yet animated atmosphere.</p> 
<p>The menu offers creative, simple, high quality dishes that can either be served individually or for sharing. These dishes have been created to embrace the cocktail offering.</p>',
            'house_of_operation' => '15:00 – 01:00 (Monday - Friday)<br/>
17:00 – 01:00 (Saturday - Sunday)',
            'dress_code' => 'Smart casual',
            'how_to_reserve' => 'Suggested but not required',
            'menu_list_url' => null,
            'image_url' => $path . '04.jpg',
        ]);
        Restaurant::create([
            'name' => 'The Ruby Fish',
            'description' => '<p>This exciting dining option offers an Estrela themed urban café where various components overlap; barista, gourmet retail, bakery & bistro. A meeting place, reading place, and connecting place with high technology to enable ease of doing business and interaction with colleagues and friends in an urban and relaxed atmosphere. Allowing you to stay connected, maximize your time, and enjoy the company of likeminded guests.</p>',
            'house_of_operation' => 'Deli opens from 06:30 - 20:00<br/>
Restaurant opens from 12:00 - 22:00',
            'dress_code' => 'Smart casual',
            'how_to_reserve' => 'Not required',
            'menu_list_url' => null,
            'image_url' => $path . '05.jpg',
        ]);
        Restaurant::create([
            'name' => 'The Momument',
            'description' => '<p>The Momument presents modern European cuisine with Asian influences using the best of locally sourced produce.  Led by Chef Niel Armstrong, the restaurant offers an exquisite a la carte selection where every presentation is to please the eyes and taste buds of the beholder.</p>
<p>Inspired by its name, Chef Niel also prepare a tasting set for guest to sample the best with the Chef’s point of view set menu.</p>
<p>Elevate your dining experience with our collection of wines and culinary cocktails while taking in the superb city view of Ukheegrego, 22 floors above the ground.</p>
<p>Everything just tastes better up here...</p>',
            'house_of_operation' => '17:00 - 23:00',
            'dress_code' => 'Smart casual',
            'how_to_reserve' => 'Required',
            'menu_list_url' => null,
            'image_url' => $path . '06.jpg',
        ]);
        Restaurant::create([
            'name' => 'Nirvana',
            'description' => '<p>Nirvana is a live music restaurant that offers luxurious yet relaxing atmosphere. Situated on the third floor of Luxuria Hotel, Nirvana delivers an excellent dining experience through delectable Xesmoaji – Etrana fusion cuisine coupled with high quality entertainment.</p>
<p> Partnered with Embers, Ylowraka’s number one jazz music venue, Nirvana showcases both local and well-known international artists that satisfy its audiences.  It is the rendezvous for sophisticated adults who seek for an authentic and elevated experience.</p>',
            'house_of_operation' => 'Monday to Saturday from 06:00 PM to 12:30 AM <br/>
Closed on Sunday',
            'dress_code' => 'Smart casual',
            'how_to_reserve' => 'Call 202-555-0199',
            'menu_list_url' => null,
            'image_url' => $path . '07.jpg',
        ]);
        Restaurant::create([
            'name' => 'Piccolo',
            'description' => '<p>Piccolo is an innovative re-imagining of traditional Ogrurg food as gourmet cuisine. Using precision modern cooking techniques and fresh, premium quality natural ingredients, Piccolo strives to create a fine dining experience that is recognizably and authentically Ogrurg.</p>
<p>Elevating familiar dishes into exquisitely flavorful and colorful creations through the alchemy of its kitchen, the recipes created by Piccolo juxtapose the old and new, the common and the unexpected of Ogrurg cooking.</p>',
            'house_of_operation' => '11:00 - 22:00 (Lounge) <br/>
11:00 - 15:30 (Lunch)<br/>
 18:30 - 22:00 (Dinner)',
            'dress_code' => 'Smart casual',
            'how_to_reserve' => 'For reservations, pelase call 202-555-0192',
            'menu_list_url' => null,
            'image_url' => $path . '08.jpg',
        ]);
        Restaurant::create([
            'name' => 'The Incredible Pizzeria',
            'description' => '<p>The Incredible Pizzeria offers both the freshest, most delicious Agrya cuisine in a wonderfully serene setting. The menu is crafted true to Agrya culinary tradition from its stunning Chesa, Ouveekrough and Zinton live stations as well as its splendid private dining rooms. Led by Executive Chef Rolodon Zury, be delighted with the diverse range of Agrya cuisine offered including Pathe, Esririe, Coucrouis and Frorolis.</p>',
            'house_of_operation' => 'Lunch : 11.30 – 14.00 (last order) <br/>
Dinner : 18.00 – 22.00 (last order)',
            'dress_code' => 'Formal or smart casual',
            'how_to_reserve' => 'For reservations, please call 207-555-0151',
            'menu_list_url' => null,
            'image_url' => $path . '09.jpg',
        ]);
        Restaurant::create([
            'name' => 'The Mad Fish',
            'description' => '<p>Enlivening the taste of Wabraria’s Michelin-starred gastronomic haven, The Mad Fish reintroduces its distinguishing excellence with a contemporary twist. With a wide, delectable array of Ablurg delicacies, we aim to be synonymous with a constantly exceptional quality and service. Our highly-trained staff and personalized service awaits to suit your needs of the moment. In The Mad Fish, find the balance of refined taste and experience that enhances one another.</p>
<p>Nestled in an exquisite setting at Luxuria Hotel in the strategic compound, it is ready to dish out to over 250 guests from business associates as well as families. The restaurant also accommodates 12 private rooms for the city’s diners who wish to banquet comfortably for special occasions.</p>',
            'house_of_operation' => '<p><b>Lunch:  </b><br/>
Monday to Saturday - 11:00am to 3:00pm <br/>
Sunday - 10:00am to 3:00pm<br/></p>
<p><b>Dinner:  </b><br/>
Monday to Sunday - 6:00pm to 11:00pm</p>',
            'dress_code' => 'Smart casual',
            'how_to_reserve' => 'For reservations or private function inquiries, please call 207-555-0199',
            'menu_list_url' => null,
            'image_url' => $path . '10.jpg',
        ]);
    }
}
