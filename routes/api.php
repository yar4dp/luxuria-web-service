<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


/**
 * API Routes for Customer Application
 */

Route::post('customer/register', 'CustomerController@register')->middleware('application');

Route::post('customer/login', 'CustomerController@login')->middleware('application');

Route::post('customer/profile', 'CustomerController@profile')->middleware('application');

Route::post('customer/reservation/list', 'ReservationController@listByCustomer')->middleware('application');

Route::post('customer/reservation/create', 'ReservationController@createByCustomer')->middleware('application');

Route::post('customer/reservation/update', 'ReservationController@updateByCustomer')->middleware('application');


/**
 * API Routes for Employee Application
 */

Route::post('employee/register', 'EmployeeController@register')->middleware('application');

Route::post('employee/login', 'EmployeeController@login')->middleware('application');

Route::post('employee/profile', 'EmployeeController@profile')->middleware('application');

Route::post('employee/reservation/list', 'ReservationController@listOfUnapprovedReservations')->middleware('application');

Route::post('employee/reservation/list-by-date', 'ReservationController@listOfReservationsByDate')->middleware('application');

Route::post('employee/reservation/list/approved', 'ReservationController@listOfApprovedReservations')->middleware('application');

Route::post('employee/reservation/list/rejected', 'ReservationController@listOfRejectedReservations')->middleware('application');

Route::post('employee/reservation/approve', 'ReservationController@approve')->middleware('application');

Route::post('employee/reservation/reject', 'ReservationController@reject')->middleware('application');


/**
 * API Routes for Application
 */

Route::get('facility', 'FacilityController@index')->middleware('application');

Route::get('restaurant', 'RestaurantController@index')->middleware('application');

Route::get('room', 'RoomController@index')->middleware('application');

Route::post('estimated-rate', 'ReservationController@checkEstimatedRate')->middleware('application');


/**
 * API Routes for Development Purpose, need "Superuser" privilege
 */

Route::delete('customer/delete', 'CustomerController@delete');

Route::resource('customers', 'CustomerController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);

Route::resource('employees', 'EmployeeController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);

Route::resource('facilities', 'FacilityController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);

Route::resource('restaurants', 'RestaurantController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);

Route::resource('rooms', 'RoomController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);

Route::resource('reservations', 'ReservationController', ['only' => 'index', 'store', 'show', 'update', 'destroy']);