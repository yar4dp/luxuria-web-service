<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EmployeeTest extends TestCase
{
    private $email = 'employee01@hotel-xyz.id';
    private $password = 'rahasia';

    /**
     * Employee login test.
     *
     * @return String
     */
    public function testEmployeeCanLogin()
    {
        $response = $this->json('POST', '/api/employee/login', [
            'email' => $this->email,
            'password' => $this->password,
        ], [
            'x-api-key' => env('API_KEY'),
        ]);

        $response->assertJson([
            'status' => 200,
        ]);

        $response->assertJsonStructure([
            'status',
            'data' => [
                'token'
            ],
        ]);

        $token = $response->json()['data']['token'];

        $this->assertStringStartsWith('$2y$10$', $token);

        $this->assertDatabaseHas('users', [
            'token' => $token,
        ]);

        return $token;
    }

    /**
     * Employee profile test.
     *
     * @depends testEmployeeCanLogin
     * @param $token
     */
    public function testEmployeeCanGetTheirProfile($token)
    {
        $response = $this->json('POST', '/api/employee/profile', [], [
            'x-api-key' => env('API_KEY'),
            'Authorization' => $token,
        ]);

        $response->assertJson([
            'status' => 200,
        ]);

        $response->assertJsonStructure([
            'status',
            'data' => [
                'first_name',
                'last_name',
                'phone_number',
                'user' => [
                    'email',
                ],
            ]
        ]);

        $email = $response->json()["data"]["user"]["email"];

        $this->assertStringStartsWith($this->email, $email);
    }
}
