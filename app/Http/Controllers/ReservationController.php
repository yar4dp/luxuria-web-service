<?php

namespace App\Http\Controllers;

use App\Approval;
use App\Customer;
use App\Employee;
use App\HighSeason;
use App\Reservation;
use App\Response\Response;
use App\Room;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class ReservationController extends Controller
{
    use Response;

    /**
     * ReservationController constructor.
     */
    public function __construct()
    {
        $this->middleware('customer')->only(['listByCustomer', 'createByCustomer', 'updateByCustomer']);

        $this->middleware('employee')->only(['listOfUnapprovedReservations', 'approve', 'reject', 'listOfApprovedReservations', 'listOfRejectedReservations, listOfReservationsByDate']);

        $this->middleware('superuser')->only(['index', 'store', 'show', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->success(Reservation::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->storeValidation());

        if ($validator->fails()) {
            return $this->badRequest($validator->errors()->first());
        }

        if ($this->checkInBeforeExpectedTime($request->check_in, '14:00')) {
            return $this->badRequest('If you really need to check in before 14:00, please select the previous day and set check in time after 14:00.');
        }

        if ($this->checkOutAfterExpectedTime($request->check_out, '12:00')) {
            return $this->badRequest('If you really need to check out after 12:00, please select the next day and set check out time before 12:00.');
        }

        $room = Room::where('id', $request->room_id)->first();

        $estimatedRate = $this->calculateEstimatedRate($request->check_in, $request->check_out, $room->rate);

        $request->request->add(['estimated_rate' => $estimatedRate]);

        $reservation = Reservation::create($request->all());

        return $this->success($reservation, 'Reservation created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Reservation $reservation)
    {
        return $this->success($reservation, 'Reservation detail.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Reservation $reservation)
    {
        $reservation->update($request->all());
        return $this->success($reservation, 'Reservation updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Reservation $reservation)
    {
        $reservation->delete();
        return $this->success($reservation, 'Reservation removed.');
    }

    /**
     * List reservations by customer.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function listByCustomer()
    {
        $customer = Customer::where('user_id', Auth::user()->id)->first();
        $reservations = Reservation::where('created_by', $customer->id)->orderBy('id', 'desc')->paginate();
        return $this->success($reservations, 'Reservation list by user.');
    }

    /**
     * Customer create a reservation.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createByCustomer(Request $request)
    {
        $customer = Customer::where('user_id', Auth::user()->id)->first();
        $request->request->add(['created_by' => $customer->id]);
        return $this->store($request);
    }

    /**
     * Customer update a reservation.
     *
     * @param Request $request
     */
    public function updateByCustomer(Request $request)
    {
        $validator = Validator::make($request->all(), $this->updateValidation());

        if ($validator->fails()) {
            return $this->badRequest($validator->errors()->first());
        }

        $approval = Approval::where('reservation_id', $request->id)->first();
        if ($approval != null) {
            return $this->forbidden('You can\'t update your reservation after it was accepted.');
        }

        $reservation = Reservation::where('id', $request->id)->first();

        return $this->update($request, $reservation);
    }

    /**
     * Handle check estimated rate request.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkEstimatedRate(Request $request)
    {
        $validator = Validator::make($request->all(), $this->checkEstimatedRateValidation());

        if ($validator->fails()) {
            return $this->badRequest($validator->errors()->first());
        }

        $room = Room::where('id', $request->room_id)->first();

        $estimatedRate = $this->calculateEstimatedRate($request->check_in, $request->check_out, $room->rate);

        $data = new \StdClass();
        $data->estimated_rate = $estimatedRate;
        return $this->success($data, 'Estimated rate.');
    }

    /**
     * List of unapproved reservation.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function listOfUnapprovedReservations()
    {
        $unapprovedReservation = Reservation::where('approval_id', '=', null)->paginate();
        return $this->success($unapprovedReservation, 'List of unapproved reservations.');
    }

    /**
     * List of approved reservation.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function listOfApprovedReservations()
    {
        $approvedReservation = Reservation::where('approval_id', '!=', null)
            ->whereHas('Approval', function ($q) {
                $q->where('status', 1);
            })->paginate();
        return $this->success($approvedReservation, 'List of approved reservations.');
    }

    /**
     * List of rejected reservation.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function listOfRejectedReservations()
    {
        $rejectedReservation = Reservation::where('approval_id', '!=', null)
            ->whereHas('Approval', function ($q) {
                $q->where('status', 0);
            })->paginate();
        return $this->success($rejectedReservation, 'List of rejected reservations.');
    }

    /**
     * Approve or reject reservation.
     *
     * @param Request $request
     * @param bool $status
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function approve(Request $request, $status = true, $message = 'Reservation approved.')
    {
        $request->request->add(['status' => $status]);

        $validator = Validator::make($request->all(), $this->approvalValidation());

        if ($validator->fails()) {
            return $this->badRequest($validator->errors()->first());
        }

//        $reservation = Reservation::find($request->reservation_id)->first();
        $reservation = Reservation::where('id', $request->reservation_id)->first();

        if ($reservation->approval_id != null) {
            return $this->badRequest('Reservation has been approved or rejected.');
        }

        $employee = Employee::find(Auth::user()->id)->first();
        $request->request->add(['created_by' => $employee->id]);

        $approval = Approval::create($request->all());

        $reservation->approval_id = $approval->id;
        $reservation->save();

        return $this->success($reservation, $message);
    }

    /**
     * Reject reservation.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reject(Request $request)
    {
        return $this->approve($request, false, 'Reservation rejected.');
    }

    /**
     * Validator array for an incoming store request.
     *
     * @return array
     */
    private function storeValidation()
    {
        return array_merge($this->checkEstimatedRateValidation(), [
            'created_by' => 'required|exists:customers,id',
            'guest_first_name' => 'required',
            'guest_last_name' => 'required',
            'guest_address' => 'required',
            'guest_phone_number' => 'required',
        ]);
    }

    /**
     * Validation array for an update store request.
     *
     * @return array
     */
    private function updateValidation()
    {
        return [
            'id' => 'required|exists:reservations',
            'room_id' => 'exists:rooms,id',
            'total_person' => 'numeric|max:4',
            'check_in' => 'date_format:"Y-m-d H:i:s"|after:now',
            'check_out' => 'date_format:"Y-m-d H:i:s"|after:check_in',
        ];
    }

    /**
     * Validation array for an approval request.
     *
     * @return array
     */
    public function approvalValidation()
    {
        return [
            'note' => 'required',
            'reservation_id' => 'required|exists:reservations,id',
        ];
    }

    /**
     * Validator array for an incoming check estimated rate request.
     *
     * @return array
     */
    public function checkEstimatedRateValidation()
    {
        return [
            'room_id' => 'required|exists:rooms,id',
            'total_person' => 'required|numeric|max:4',
            'check_in' => 'required|date_format:"Y-m-d H:i:s"|after:now',
            'check_out' => 'required|date_format:"Y-m-d H:i:s"|after:check_in',
        ];
    }

    /**
     * Calculate estimated rate.
     *
     * @param $check_in
     * @param $check_out
     * @param $rate
     * @return int
     */
    private function calculateEstimatedRate($check_in, $check_out, $rate)
    {
        $check_in = Carbon::parse($check_in);
        $check_out = Carbon::parse($check_out);

        $estimatedRate = 0;
        do {
            $highSeason = HighSeason::where('calendar_date', $check_in->format('Y-m-d'))->first();
            $additionalRate = 0;
            if ($highSeason != null) {
                $additionalRate = $highSeason->additional_rate;
            }
            $estimatedRate += $rate + ($additionalRate * $rate);
            $check_in = $check_in->addDay(1);
        } while ($check_in->lessThanOrEqualTo($check_out));

        return $estimatedRate;
    }

    /**
     * Determine if customer check in before expected time.
     *
     * @param $checkIn
     * @param $expectedTime
     * @return bool
     */
    private function checkInBeforeExpectedTime($checkIn, $expectedTime)
    {
        $checkIn = Carbon::parse($checkIn)->format('H:i');
        $expectedTimeToCheckIn = Carbon::parse($expectedTime);
        if ($expectedTimeToCheckIn->greaterThan(Carbon::parse($checkIn))) {
            return true;
        }
        return false;
    }

    /**
     * Determine if customer check out after expected time.
     *
     * @param $checkOut
     * @param $expectedTime
     * @return bool
     */
    private function checkOutAfterExpectedTime($checkOut, $expectedTime)
    {
        $checkOut = Carbon::parse($checkOut)->format('H:i');
        $expectedTimeToCheckOut = Carbon::parse($expectedTime);
        if ($expectedTimeToCheckOut->lessThan(Carbon::parse($checkOut))) {
            return true;
        }
        return false;
    }

    /**
     * List reservations by date
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function listOfReservationsByDate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'field' => 'required',
            'from' => 'required|date_format:Y-m-d H:i:s',
            'to' => 'required|date_format:Y-m-d H:i:s',
        ]);

        if ($validator->fails()) {
            return $this->badRequest($validator->errors()->first());
        }

        $reservations = Reservation::all()->where($request->field, '>=', $request->from)
            ->where($request->field, '<=', $request->to);

        return $this->success($reservations, "Reservations list from " . $request->from . " to " . $request->to);
    }

}
