<?php

namespace App\Http\Middleware;

use App\Response\Response;
use Closure;

class SuperuserOperation
{
    use Response;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->hasHeader('x-superuser-key')) {
            if ($request->header('x-superuser-key') == env('SUPERUSER_KEY')) {
                return $next($request);
            }
            return $this->forbidden('You don\'t have enough power to do this operation!');
        }
        return $this->forbidden('You don\'t have enough power to do this operation!');
    }
}
