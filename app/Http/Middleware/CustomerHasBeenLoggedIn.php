<?php

namespace App\Http\Middleware;

use App\Customer;
use App\Response\Response;
use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class CustomerHasBeenLoggedIn
{
    use Response;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->hasHeader('Authorization')) {
            $user = User::where('token', $request->header('Authorization'))->first();

            if ($user == null) {
                return $this->unauthorizedResponse();
            }

            $customer = Customer::where('user_id', $user->id)->first();
            if ($customer != null) {
                Auth::login($user);
                return $next($request);

            }

            return $this->unauthorizedResponse();
        }
        return $this->unauthorizedResponse();
    }

    private function unauthorizedResponse()
    {
        return $this->unauthorized('Please login as a customer.');
    }
}
