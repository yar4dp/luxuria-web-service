<?php

namespace App\Http\Middleware;

use App\Response\Response;
use Closure;

class AuthorizedApplication
{
    use Response;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->hasHeader('x-api-key')) {
            if ($request->header('x-api-key') == env('API_KEY')) {
                return $next($request);
            }
            return $this->forbidden('You don\'t have permission to do this operation!');
        }
        return $this->forbidden('You don\'t have permission to do this operation!');
    }
}
