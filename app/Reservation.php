<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = [
        'created_by',
        'room_id',
        'total_person',
        'check_in',
        'check_out',
        'estimated_rate',
        'additional_request',
        'guest_first_name',
        'guest_last_name',
        'guest_address',
        'guest_phone_number',
    ];

    protected $hidden = ['approval_id', 'created_by', 'room_id', 'created_at'];

    public function createdBy()
    {
        return $this->belongsTo('App\Customer', 'created_by');
    }

    public function room()
    {
        return $this->belongsTo('App\Room');
    }

    public function approval()
    {
        return $this->belongsTo('App\Approval');
    }

    public function toArray()
    {
        $array = parent::toArray();
        $array['created_by'] = $this->createdBy;
        $array['room'] = $this->room;
        $array['approval'] = $this->approval;
        return $array;
    }
}
