<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'user_id', 'first_name', 'last_name', 'phone_number',
    ];

    protected $hidden = [
        'id', 'user_id', 'created_at', 'updated_at',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function toArray()
    {
        $array = parent::toArray();
        $array['user'] = $this->user;
        return $array;
    }
}
