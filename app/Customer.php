<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'user_id', 'first_name', 'last_name', 'birth_date', 'address', 'phone_number',
    ];

    protected $hidden = [
        'user_id', 'created_at', 'updated_at', 'id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function toArray()
    {
        $array = parent::toArray();
        $array['user'] = $this->user;
        return $array;
    }
}
