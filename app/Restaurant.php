<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $fillable = ['name', 'description', 'house_of_operation', 'dress_code', 'how_to_reserve', 'menu_list_url', 'image_url'];

    protected $hidden = ['id', 'created_at', 'updated_at'];
}
