<?php
namespace App\Response;

use Illuminate\Http\JsonResponse;

trait Response
{

    /**
     * Success JsonResponse.
     *
     * Standard response for successful HTTP requests.
     *
     * @param $data
     * @param String $message
     * @param int $statusCode
     * @return JsonResponse
     */
    public function success($data, $message = null, $statusCode = 200)
    {
        $response = new ResponseModel();
        $response->status = $statusCode;
        $response->message = $message;
        $response->data = $data;

        return JsonResponse::create($response);
    }

    /**
     * Bad request JsonResponse.
     *
     * The server cannot or will not process the request due to an apparent client error
     * (e.g., malformed request syntax, size too large, invalid request message framing, or deceptive request routing).
     *
     * @param String $message
     * @param int $statusCode
     * @return JsonResponse
     */
    public function badRequest($message, $statusCode = 400)
    {
        $response = new ResponseModel();
        $response->status = $statusCode;
        $response->message = $message;

        return JsonResponse::create($response);
    }

    /**
     * Unauthorized JsonResponse
     *
     * Similar to 403 Forbidden, but specifically for use when authentication is required and has failed or
     * has not yet been provided.
     *
     * @param String $message
     * @param int $statusCode
     * @return JsonResponse
     */
    public function unauthorized($message, $statusCode = 401)
    {
        $response = new ResponseModel();
        $response->status = $statusCode;
        $response->message = $message;

        return JsonResponse::create($response);
    }

    /**
     * Forbidden JsonResponse
     *
     * The request was valid, but the server is refusing action.
     * The user might not have the necessary permissions for a resource, or may need an account of some sort.
     *
     * @param String $message
     * @param int $statusCode
     * @return JsonResponse
     */
    public function forbidden($message, $statusCode = 403)
    {
        $response = new ResponseModel();
        $response->status = $statusCode;
        $response->message = $message;

        return JsonResponse::create($response);
    }

    /**
     * Not found JsonResponse.
     *
     * The requested resource could not be found but may be available in the future.
     *
     * @param String $message
     * @param int $statusCode
     * @return JsonResponse
     */
    public function notFound($message, $statusCode = 404)
    {
        $response = new ResponseModel();
        $response->status = $statusCode;
        $response->message = $message;

        return JsonResponse::create($response);
    }

}